<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $guarded = [];
    protected $appends = ['user'];

    public function posting_datetime(){
        $created_date_time = date('d-m-Y H:i:s', strtotime($this->created_at) ); 
        return $created_date_time;
    }

    public function getUserAttribute()
    {
      return  $user = User::select(['name','photo','id'])->where('id',$this->user_id)->first();
    }
}
