<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Bid;
use App\Category;
use App\Contact_query;
use App\Slider;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
    public static function allCategory()
    {
        $top_categories = Category::whereCategoryType('auction')->orderBy('category_name', 'asc')->get();
        $dateNow = date("Y-m-d H:i:s");
        $product_view = Ad::where('status','=','1')->where('expired_at','>',$dateNow)->orderby('count_view','DESC')->limit(3)->get();
        foreach ($product_view as $key => $value) {
            $bids_flag = Bid::select("bid_amount")->where('bids.ad_id','=',$value['id'])->orderby('bids.id','DESC')->first();

            $diff     = strtotime($value['expired_at'])-time();
            $product_view[$key]["expired_second"] = $diff;
            if($bids_flag){
                $product_view[$key]["current_bid"] = $bids_flag['bid_amount'];
            }else{
                $product_view[$key]["current_bid"] = $value['price'];
            }


        }
        return view('ver2.sticker.categoryall', compact('product_view','top_categories'));
    }

}
