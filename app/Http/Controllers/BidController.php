<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Bid;
use App\User;
use App\HB;
use App\Notify;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Config;
use App\Helpers\FirebaseService;

use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class BidController extends Controller
{
    public function index($ad_id){
        $user = Auth::user();
        $user_id = $user->id;
        $ad = Ad::find($ad_id);

        $title = trans('app.bids_for').' '.$ad->title;

        if (! $user->is_admin()){
            if ($ad->user_id != $user_id){
                return view('admin.error.error_404');
            }
        }
        return view('admin.bids', compact('title', 'ad'));
    }
    public function postBidAjax(Request $request , $ad_id){
        if ( ! Auth::check()){

              return $this->dataError(trans('app.login_first_to_post_bid'),[],200);
        }

        $host = '';
        $user = Auth::user();
        $bid_amount = $request->bid_amount;

        $ad = Ad::find($ad_id);
        $current_max_bid = $ad->current_bid();

        if ($bid_amount <= $current_max_bid ){
              return $this->dataError("Vui lòng nhập số tiền giá thầu không nhỏ hơn hoặc bằng ".number_format(($current_max_bid + $ad->next_min), 0).' đ',[],200);
        }
        $ads = Ad::find($ad_id);
        $image = Media::where('ad_id','=',$ad_id)->first();
        if($image){
          $url_image =  $image->media_name;
        }else{
          $url_image = '';
        }
        if($bid_amount >= $ads->buy_now ){

              $data_message = [];
              $bid_first = Bid::where('ad_id','=',$ad_id)->where('user_id','!=',Auth::user()->id)->first();
              if($bid_first){
                  $data_message[] = [
                      'image'         => $url_image,
                      'title'         => "Có người đấu giá cao hơn.",
                      'user_id'       => $bid_first->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 1
                  ];
                  $user_push = User::where('id',$bid_first->user_id)->first();
                  try{
                      FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                          'image'         => $url_image,
                          'title'         => "Có người đấu giá cao hơn.",
                          'user_id'       => $bid_first->user_id,
                          'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                          'time'          => date("d-m-Y H:i:s"),
                          'ad_id' => $ad_id,
                          'type' => 1
                      ],'test');
                  }
                  catch (\Exception $exception)
                  {
                      
                  }


                  $new_notify = new Notify();
                  $new_notify->user_id = $bid_first->user_id;
                  $new_notify->data = json_encode([
                      'image'         => $url_image,
                      'title'         => "Có người đấu giá cao hơn.",
                      'user_id'       => $bid_first->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 1
                  ]);
                  $new_notify->create_date = date("Y-m-d H:i:s") ;
                  $new_notify->view =0 ;
                  $new_notify->save();

              }
              $bid = new Bid();
              $bid->user_id = Auth::user()->id;
              $bid->ad_id = $ad_id;
              $bid->is_accepted = 1;
              $bid->bid_amount = $request->bid_amount;
              $bid->save();
              $ads->expired_at = date("Y-m-d H:i:s", strtotime("-1 days"));
              $ads->flag = 1;
              $ads->save();

              $data_message[] = [
                  'image'         => $url_image,
                  'title'         => "Chúc mừng! Bạn đã thắng đấu giá",
                  'user_id'       => Auth::user()->id,
                  'description'    => "Bạn đã chiến thắng đấu giá ".$ads->title."!",
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 3
              ];
              $user_push = User::where('id',Auth::user()->id)->first();
              try{
                  FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                      'image'         => $url_image,
                      'title'         => "Chúc mừng! Bạn đã thắng đấu giá",
                      'user_id'       => Auth::user()->id,
                      'description'    => "Bạn đã chiến thắng đấu giá ".$ads->title."!",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 3
                  ],'test');
              }
              catch (\Exception $exception)
              {

              }


              $new_notify = new Notify();
              $new_notify->user_id = Auth::user()->id;
              $new_notify->data = json_encode([
                  'image'         => $url_image,
                  'title'         => "Chúc mừng! Bạn đã thắng đấu giá",
                  'user_id'       => Auth::user()->id,
                  'description'    => "Bạn đã chiến thắng đấu giá ".$ads->title."!",
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 3
              ]);
              $new_notify->create_date = date("Y-m-d H:i:s") ;
              $new_notify->view =0 ;
              $new_notify->save();

              $data_message[] = [
                  'image'         => $url_image,
                  'title'         => "Đấu giá kết thúc.",
                  'user_id'       => $ads->user_id,
                  'description'    => "Đấu giá ".$ads->title." đã kết thúc",
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 2
              ];

              $user_push = User::where('id',$ads->user_id)->first();
              try{
                FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                    'image'         => $url_image,
                    'title'         => "Đấu giá kết thúc.",
                    'user_id'       => $ads->user_id,
                    'description'    => "Đấu giá ".$ads->title." đã kết thúc",
                    'time'          => date("d-m-Y H:i:s"),
                    'ad_id' => $ad_id,
                    'type' => 2
                ],'test');
              }
              catch (\Exception $exception)
              {

              }

              $new_notify = new Notify();
              $new_notify->user_id = $ads->user_id;
              $new_notify->data = json_encode([
                  'image'         => $url_image,
                  'title'         => "Đấu giá kết thúc.",
                  'user_id'       => $ads->user_id,
                  'description'    => "Đấu giá ".$ads->title." đã kết thúc",
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 2
              ]);
              $new_notify->create_date = date("Y-m-d H:i:s") ;
              $new_notify->view =0 ;
              $new_notify->save();


              $HB = HB::where('user_id','=',Auth::user()->id)->where('auction_id','=',$ad_id)->first();
              if(!$HB){
                    $HB = new HB();
                    $HB->user_id = Auth::user()->id;
                    $HB->auction_id = $ad_id;
                    $HB->save();
              }

              $data = [
                  'data_message' => $data_message,
                  'ad_id'         => $ad_id,
                  'user_id'       => $user->id,
                  'bid_amount'    => $bid_amount,
                  'is_accepted'   => 1,
                  'current_bid_display' => number_format($bid_amount, 0).' đ'
              ];
              $data['create_ad'] = date("d-m-Y H:i:s");
              return $this->dataSuccess(trans('app.your_bid_posted'),$data,200);

        }else{
            $data_message = [];
            $bid_first = Bid::where('ad_id','=',$ad_id)->where('user_id','!=',Auth::user()->id)->first();
            if($bid_first){
              $data_message[] = [
                  'image'         => $url_image,
                  'title'         => "Có người đấu giá cao hơn.",
                  'user_id'       => $bid_first->user_id,
                  'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 1
              ];
              $user_push = User::where('id',$bid_first->user_id)->first();
              try{
                  FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                      'image'         => $url_image,
                      'title'         => "Có người đấu giá cao hơn.",
                      'user_id'       => $bid_first->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 1
                  ],'test');
              }
              catch (\Exception $exception)
              {

              }


              $new_notify = new Notify();
              $new_notify->user_id = $bid_first->user_id;
              $new_notify->data = json_encode([
                  'image'         => $url_image,
                  'title'         => "Có người đấu giá cao hơn.",
                  'user_id'       => $bid_first->user_id,
                  'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 1
              ]);
              $new_notify->create_date = date("Y-m-d H:i:s") ;
              $new_notify->view =0 ;
              $new_notify->save();

            }
            $bid = new Bid();
            $bid->user_id = Auth::user()->id;
            $bid->ad_id = $ad_id;
            $bid->is_accepted = 0;
            $bid->bid_amount = $request->bid_amount;
            $bid->save();
            $data_message[] = [
                'image'         => $url_image,
                'title'         => "Có người đấu giá.",
                'user_id'       => $ads->user_id,
                'description'    => "Đấu giá ".$ads->title." vừa mới được đấu với giá : ".number_format($request->bid_amount, 0).' đ',
                'time'          => date("d-m-Y H:i:s"),
                'ad_id' => $ad_id,
                'type' => 4
            ];
            $user_push = User::where('id',$ads->user_id)->first();
            try{
              FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                  'image'         => $url_image,
                  'title'         => "Có người đấu giá.",
                  'user_id'       => $ads->user_id,
                  'description'    => "Đấu giá ".$ads->title." vừa mới được đấu với giá : ".number_format($request->bid_amount, 0).' đ',
                  'time'          => date("d-m-Y H:i:s"),
                  'ad_id' => $ad_id,
                  'type' => 4
              ],'test');
            }
            catch (\Exception $exception)
            {

            }


            $new_notify = new Notify();
            $new_notify->user_id = $ads->user_id;
            $new_notify->data = json_encode([
                'image'         => $url_image,
                'title'         => "Có người đấu giá.",
                'user_id'       => $ads->user_id,
                'description'    => "Đấu giá ".$ads->title." vừa mới được đấu với giá : ".number_format($request->bid_amount, 0).' đ',
                'time'          => date("d-m-Y H:i:s"),
                'ad_id' => $ad_id,
                'type' => 4
            ]);
            $new_notify->create_date = date("Y-m-d H:i:s") ;
            $new_notify->view =0 ;
            $new_notify->save();


            $HB = HB::where('user_id','=',Auth::user()->id)->where('auction_id','=',$ad_id)->first();
            if(!$HB){
                  $HB = new HB();
                  $HB->user_id = Auth::user()->id;
                  $HB->auction_id = $ad_id;
                  $HB->save();
            }
            $data = [
                'data_message' => $data_message,
                'ad_id'         => $ad_id,
                'user_id'       => $user->id,
                'bid_amount'    => $bid_amount,
                'is_accepted'   => 0,
                'current_bid_display' => number_format($bid_amount, 0).' đ'
            ];
            $data['create_ad'] = date("d-m-Y H:i:s");
            return $this->dataSuccess(trans('app.your_bid_posted'),$data,200);
        }



    }

    public function postBid(Request $request , $ad_id){
        if ( ! Auth::check()){
            return redirect(route('login'))->with('error', trans('app.login_first_to_post_bid'));
        }
        $user = Auth::user();
        $bid_amount = $request->bid_amount;

        $ad = Ad::find($ad_id);
        $current_max_bid = $ad->current_bid();

        if ($bid_amount <= $current_max_bid ){
            return back()->with('error', sprintf(trans('app.enter_min_bid_amount'), themeqx_price($current_max_bid)) );
        }

        $data = [
            'ad_id'         => $ad_id,
            'user_id'       => $user->id,
            'bid_amount'    => $bid_amount,
            'is_accepted'   => 0,
        ];


        Bid::create($data);
        return back()->with('success', trans('app.your_bid_posted'));
    }

    public function bidAction(Request $request){
        $action = $request->action;
        $ad_id = $request->ad_id;
        $bid_id = $request->bid_id;

        $user = Auth::user();
        $user_id = $user->id;
        $ad = Ad::find($ad_id);

        if (! $user->is_admin()){
            if ($ad->user_id != $user_id){
                return ['success' => 0];
            }
        }

        $bid = Bid::find($bid_id);
        switch ($action){
            case 'accept':
                $bid->is_accepted = 1;
                $bid->save();

                $ads = Ad::find($request->ad_id);
                $ads->expired_at = date("Y-m-d", strtotime("-1 days"));
                $ads->save();
                break;
            case 'delete':
                $bid->delete();
                break;
        }
        return ['success' => 1];
    }

    public function bidderInfo($bid_id){
        $bid = Bid::find($bid_id);
        $title = trans('app.bidder_info');

        $auth_user = Auth::user();
        $user_id = $auth_user->id;
        $ad = Ad::find($bid->ad_id);

        if (! $auth_user->is_admin()){
            if ($ad->user_id != $user_id){
                return view('admin.error.error_404');
            }
        }

        $user = User::find($bid->user_id);

        return view('admin.profile', compact('title', 'user'));
    }

    public function store(Request $request)
    {
        try{
            $auctions = null;

            $title = $request->title;
            $slug = unique_slug($this->convert_slug($title));
            $data = [
                'title'             => $request->title,
                'slug'              => $slug,
                'description'       => $request->ad_description,
                'category_id'       => $request->category_id,
                'type'              => $request->type,
                'price'             => $request->price,

                'seller_name'       => Auth::user()->name,
                'seller_email'      => Auth::user()->email,
                'seller_phone'      => Auth::user()->phone,
                'country_id'        => Auth::user()->country_id,
                'address'           => Auth::user()->address,

                'category_type'     => 'auction',
                'status'            => '0',
                'user_id'           => Auth::user()->id,
                'expired_at'        => $request->date,
                'next_min'        => $request->step,
                'buy_now'        => $request->price_max,
            ];


            $auctions = Ad::create($data);

            if (get_option('ads_moderation') == 'direct_publish'){
                $data['status'] = '1';
            }

            if ( ! $request->price_plan){
                $data['price_plan'] = 'regular';
            }


            $this->uploadAdsImage($request,$auctions->id);



            return $this->dataSuccess('Tạo đấu giá thành công',$auctions,200);

        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),[],200);
        }

    }


}
