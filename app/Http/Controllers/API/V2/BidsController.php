<?php

namespace App\Http\Controllers\API\V2;

use App\Ad;
use App\Bid;
use App\HB;
use App\Helpers\FirebaseService;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use App\User;
use App\Notify;
use Config;
class BidsController extends Controller
{
    // $status_bidding = { 1 : "Win" , 2 : "Lose" , 3 :  "Wating" };

    public function store(Request $request,$id)
    {

        try{
            $host = '';
            $ads = Ad::find($id);
            $ad_id = $id;
            $image = Media::where('ad_id','=',$ad_id)->first();
            if($image){
              $url_image =  $image->media_name;
            }else{
              $url_image = '';
            }


            $curent_bid = Bid::where('bids.ad_id','=',$id)->orderby('bids.id','DESC')->first();
            if($ads->flag == 1){
                return $this->dataError("Đấu giá đã kết thúc.",null,200);
            }
            // print_r($request->bid_amount);die;
            if($ads->user_id == Auth::user()->id){
                return $this->dataError("Bạn không được đấu giá chính đấu giá của bạn.",null,200);
            }
            if($curent_bid){
              if(@$curent_bid->is_accepted == 1){
                  return $this->dataError("Đấu giá đã kết thúc.",null,200);
              }
              if(@$curent_bid->bid_amount + $ads->next_min > $request->bid_amount){
                  return $this->dataErrorBids("Đấu giá tiếp theo phải lớn hơn hoặc bằng : ".number_format(($curent_bid->bid_amount + $ads->next_min), 0, '', ',') ." VNĐ",$curent_bid->bid_amount + $ads->next_min,[],200);
              }
            }else{
                if(@$ads->price + $ads->next_min > $request->bid_amount){
                    return $this->dataErrorBids("Đấu giá tiếp theo phải lớn hơn hoặc bằng : ".number_format(($ads->price + $ads->next_min), 0, '', ',') ." VNĐ",$ads->price + $ads->next_min,[],200);
                }
            }
            if($request->bid_amount >= $ads->buy_now ){


                  $data_message = [];
                  $bid_first = Bid::where('ad_id','=',$ad_id)->where('user_id','!=',Auth::user()->id)->first();
                  if($bid_first){
                      $data_message[] = [
                          'image'         => $url_image,
                          'title'         => "Có người đấu giá cao hơn.",
                          'user_id'       => $bid_first->user_id,
                          'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                          'time'          => date("d-m-Y H:i:s"),
                          'ad_id' => $ad_id,
                          'type' => 1
                      ];

                      $user_push = User::where('id',$bid_first->user_id)->first();
                      try{
                          FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                              'image'         => $url_image,
                              'title'         => "Có người đấu giá cao hơn.",
                              'user_id'       => $bid_first->user_id,
                              'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                              'time'          => date("d-m-Y H:i:s"),
                              'ad_id' => $ad_id,
                              'type' => 1
                          ],'test');
                      }
                      catch (\Exception $exception)
                      {

                      }

                      $new_notify = new Notify();
                      $new_notify->user_id = $bid_first->user_id;
                      $new_notify->data = json_encode([
                          'image'         => $url_image,
                          'title'         => "Có người đấu giá cao hơn.",
                          'user_id'       => $bid_first->user_id,
                          'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                          'time'          => date("d-m-Y H:i:s"),
                          'ad_id' => $ad_id,
                          'type' => 1
                      ]);
                      $new_notify->create_date = date("Y-m-d H:i:s") ;
                      $new_notify->view =0 ;
                      $new_notify->save();

                  }


                  $data_message[] = [
                      'image'         => $url_image,
                      'title'         => "Chúc mừng! Bạn đã thắng đấu giá",
                      'user_id'       => Auth::user()->id,
                      'description'    => "Bạn đã chiến thắng đấu giá ".$ads->title."!",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 3
                  ];
                  $user_push = User::where('id',Auth::user()->id)->first();
                  try{
                      FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                          'image'         => $url_image,
                          'title'         => "Chúc mừng! Bạn đã thắng đấu giá",
                          'user_id'       => Auth::user()->id,
                          'description'    => "Bạn đã chiến thắng đấu giá ".$ads->title."!",
                          'time'          => date("d-m-Y H:i:s"),
                          'ad_id' => $ad_id,
                          'type' => 3
                      ],'test');
                  }
                  catch (\Exception $exception)
                  {

                  }

                  $new_notify = new Notify();
                  $new_notify->user_id = Auth::user()->id;
                  $new_notify->data = json_encode([
                      'image'         => $url_image,
                      'title'         => "Chúc mừng! Bạn đã thắng đấu giá",
                      'user_id'       => Auth::user()->id,
                      'description'    => "Bạn đã chiến thắng đấu giá ".$ads->title."!",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 3
                  ]);
                  $new_notify->create_date = date("Y-m-d H:i:s") ;
                  $new_notify->view =0 ;
                  $new_notify->save();

                  $data_message[] = [
                      'image'         => $url_image,
                      'title'         => "Đấu giá kết thúc.",
                      'user_id'       => $ads->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã kết thúc",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 2
                  ];
                  $user_push = User::where('id',$ads->user_id)->first();
                  try{
                      FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                          'image'         => $url_image,
                          'title'         => "Đấu giá kết thúc.",
                          'user_id'       => $ads->user_id,
                          'description'    => "Đấu giá ".$ads->title." đã kết thúc",
                          'time'          => date("d-m-Y H:i:s"),
                          'ad_id' => $ad_id,
                          'type' => 2
                      ],'test');
                  }
                  catch (\Exception $exception)
                  {

                  }

                  $new_notify = new Notify();
                  $new_notify->user_id = $ads->user_id;
                  $new_notify->data = json_encode([
                      'image'         => $url_image,
                      'title'         => "Đấu giá kết thúc.",
                      'user_id'       => $ads->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã kết thúc",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 2
                  ]);
                  $new_notify->create_date = date("Y-m-d H:i:s") ;
                  $new_notify->view =0 ;
                  $new_notify->save();


                  $bid = new Bid();
                  $bid->user_id = Auth::user()->id;
                  $bid->ad_id = $id;
                  $bid->is_accepted = 1;
                  $bid->bid_amount = $request->bid_amount;
                  $bid->save();
                  $ads->expired_at = date("Y-m-d H:i:s", strtotime("-1 days"));
                  $ads->flag = 1;
                  $ads->save();
                  $HB = HB::where('user_id','=',Auth::user()->id)->where('auction_id','=',$id)->first();
                  if(!$HB){
                        $HB = new HB();
                        $HB->user_id = Auth::user()->id;
                        $HB->auction_id = $id;
                        $HB->save();
                  }


                  $data_return = [];
                  $data_return['bid'] = $bid;
                  $data_return['data_message'] = $data_message;
                  return $this->dataSuccessBid('Đấu giá thành công',$data_return,200,true);
            }else{



                $data_message = [];
                $bid_first = Bid::where('ad_id','=',$ad_id)->where('user_id','!=',Auth::user()->id)->first();
                if($bid_first){
                  $data_message[] = [
                      'image'         => $url_image,
                      'title'         => "Có người đấu giá cao hơn.",
                      'user_id'       => $bid_first->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 1
                  ];
                  $user_push = User::where('id',$bid_first->user_id)->first();
                  try{
                      FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                          'image'         => $url_image,
                          'title'         => "Có người đấu giá cao hơn.",
                          'user_id'       => $bid_first->user_id,
                          'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                          'time'          => date("d-m-Y H:i:s"),
                          'ad_id' => $ad_id,
                          'type' => 1
                      ],'test');
                  }
                  catch (\Exception $exception)
                  {

                  }
                  $new_notify = new Notify();
                  $new_notify->user_id = $bid_first->user_id;
                  $new_notify->data = json_encode([
                      'image'         => $url_image,
                      'title'         => "Có người đấu giá cao hơn.",
                      'user_id'       => $bid_first->user_id,
                      'description'    => "Đấu giá ".$ads->title." đã có người đấu giá cao hơn bạn.",
                      'time'          => date("d-m-Y H:i:s"),
                      'ad_id' => $ad_id,
                      'type' => 1
                  ]);
                  $new_notify->create_date = date("Y-m-d H:i:s") ;
                  $new_notify->view =0 ;
                  $new_notify->save();

                }

                $data_message[] = [
                    'image'         => $url_image,
                    'title'         => "Có người đấu giá.",
                    'user_id'       => $ads->user_id,
                    'description'    => "Đấu giá ".$ads->title." vừa mới được đấu với giá : ".number_format($request->bid_amount, 0).' đ',
                    'time'          => date("d-m-Y H:i:s"),
                    'ad_id' => $ad_id,
                    'type' => 4
                ];
                $user_push = User::where('id',$ads->user_id)->first();
                try{
                    FirebaseService::fcm($user_push->device_token,'Thông Báo','Thông báo thành công.',[
                        'image'         => $url_image,
                        'title'         => "Có người đấu giá.",
                        'user_id'       => $ads->user_id,
                        'description'    => "Đấu giá ".$ads->title." vừa mới được đấu với giá : ".number_format($request->bid_amount, 0).' đ',
                        'time'          => date("d-m-Y H:i:s"),
                        'ad_id' => $ad_id,
                        'type' => 4
                    ],'test');
                }
                catch (\Exception $exception)
                {

                }
                $new_notify = new Notify();
                $new_notify->user_id = $ads->user_id;
                $new_notify->data = json_encode([
                    'image'         => $url_image,
                    'title'         => "Có người đấu giá.",
                    'user_id'       => $ads->user_id,
                    'description'    => "Đấu giá ".$ads->title." vừa mới được đấu với giá : ".number_format($request->bid_amount, 0).' đ',
                    'time'          => date("d-m-Y H:i:s"),
                    'ad_id' => $ad_id,
                    'type' => 4
                ]);
                $new_notify->create_date = date("Y-m-d H:i:s") ;
                $new_notify->view =0 ;
                $new_notify->save();

                $bid = new Bid();
                $bid->user_id = Auth::user()->id;
                $bid->ad_id = $id;
                $bid->is_accepted = 0;
                $bid->bid_amount = $request->bid_amount;
                $bid->save();
                $HB = HB::where('user_id','=',Auth::user()->id)->where('auction_id','=',$id)->first();
                if(!$HB){
                      $HB = new HB();
                      $HB->user_id = Auth::user()->id;
                      $HB->auction_id = $id;
                      $HB->save();
                }

                $data_return = [];
                $data_return['bid'] = $bid;
                $data_return['data_message'] = $data_message;
                return $this->dataSuccessBid('Đấu giá thành công',$data_return,200,false);
            }



        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }

    }

    public function show($id)
    {
          try{
              $bids = Bid::orderby('bids.id','DESC');
              // $bids = $bids->join('users', 'users.id', '=', 'bids.user_id');
              $bids = $bids->where('bids.ad_id','=',$id);
              return $this->dataSuccess('Lấy đấu giá chi tiết thành công',$bids->get(),200);

          }
          catch (\Exception $exception)
          {
              return $this->dataError($exception->getMessage(),null,200);
          }
    }


    public function testPush(Request $request)
    {
       try{
           FirebaseService::fcm($request->device_token,'Test Push','Push thành công.',[],'test_push');
           return $this->dataSuccess('Push thành công',null,200);
       }
       catch (\Exception $exception)
       {
           return $this->dataError($exception->getMessage(),null,200);
       }
    }

    public function getBidByUser(Request $request)
    {
        try{
            $HBs = HB::where('user_id','=',Auth::user()->id);
            $HBs = $HBs->paginate(10);

            foreach ($HBs as $key => $value) {
                $bids_flag = Bid::where('ad_id','=',$value['auction_id'])->where('user_id','=',Auth::user()->id)->orderby('bids.id','DESC')->first();
                $HBs[$key]['bid'] = $bids_flag;
                $HBs[$key]['auction'] = Ad::select("id","title","description","category_id","price","expired_at","next_min","buy_now","flag")->where('id','=',$value['auction_id'])->first();
                $diff     = strtotime($HBs[$key]['auction']['expired_at'])-time();
                $HBs[$key]['auction']["expired_second"] = $diff;
                $current = Bid::select("bid_amount")->where('ad_id','=',$value['auction_id'])->first();
                if($current){
                      $HBs[$key]['auction']['current_bid'] = $current->bid_amount;
                }else{
                      $HBs[$key]['auction']['current_bid'] = $HBs[$key]['auction']['price'];
                }

                if($bids_flag['is_accepted'] == 1){
                    $HBs[$key]['status'] = 1;
                }else{
                    if($HBs[$key]['auction']['flag'] == 1){
                        $HBs[$key]['status'] = 2;
                    }else{
                        $HBs[$key]['status'] = 3;
                    }
                }
            }
            return $this->dataSuccess('Lấy danh sách đấu giá thành công',$HBs,200);


        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }

    public function getBidByUserDetail(Request $request , $id)
    {
        try{
            $bids = Bid::orderby('id','DESC');
            $bids = $bids->where('user_id','=',Auth::user()->id);
            $bids = $bids->where('ad_id','=',$id);
            $bids = $bids->get();
            $ads = Ad::select("id",'user_id',"title","description","category_id","price","expired_at","next_min","buy_now","flag")->where('id','=',$id)->first();
            $return_data = [];
            $check_win = Bid::where('ad_id','=',$id)->where('user_id','=',Auth::user()->id)->orderby('id','DESC')->first();
            if(@$check_win && @$check_win['is_accepted'] == 1){
                $return_data['status'] = 1;
                $return_data['seller'] = User::select("name", "email" , "phone" , "address", "photo")->where('id','=',$ads['user_id'])->first();
            }else{
                if($ads['flag'] == 1){
                    $return_data['status'] = 2;
                }else{
                    $return_data['status'] = 3;
                }
                $return_data['seller'] = null;
            }
            $return_data['data'] = $bids;
            return $this->dataSuccess('Lấy danh sách đấu giá thành công',$return_data,200);
        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }

}
