<?php

namespace App\Http\Controllers\API\V2;

use App\Ad;
use App\Bid;
use App\User;
use App\HB;
use App\Notify;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Config;

use App\Http\Controllers\Controller;
class NotifyController extends Controller
{

    public function notify(Request $request){
        if ( ! Auth::check()){

              return $this->dataError("Chưa đăng nhập",[],200);
        }
        $page = $request->page ? $request->page : 0;
        $notify = Notify::where('user_id','=',Auth::user()->id)->offset(($page*10))->limit(10)->orderby('id','DESC')->get();
        $notify_count = Notify::where('user_id','=',Auth::user()->id)->where('view','=',0)->orderby('id','DESC')->count();
        $data = [];
        $data['notify'] = $notify;
        $data['notify_count'] = $notify_count;
        return $this->dataSuccess("Lấy danh sách thành công",$data,200);



    }


}
