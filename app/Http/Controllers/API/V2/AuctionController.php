<?php

namespace App\Http\Controllers\API\V2;

use App\Ad;
use App\Bid;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Validator;
use App\User;

class AuctionController extends Controller
{
      public function index(Request $request)
      {
            try{
                $auctions = Ad::select("id","title","description","category_id","price","expired_at","next_min","buy_now")->orderby('id','DESC');
                if(@$request->category_id != "")
                {
                    $auctions =  $auctions->where('category_id',$request->category_id);
                }
                if (@$request->name != "")
                {
                    $auctions = $auctions->where('title','like',"%$request->name%");
                }
                $dateNow = date("Y-m-d H:i:s");
                $auctions = $auctions->where('expired_at','>=',$dateNow);
                $auctions = $auctions->paginate(10);
                foreach ($auctions as $key => $value) {
                    $bids_flag = Bid::select("bid_amount")->where('bids.ad_id','=',$value['id'])->orderby('bids.id','DESC')->first();

                    $diff     = strtotime($value['expired_at'])-time();
                    $auctions[$key]["expired_second"] = $diff;
                    if($bids_flag){
                        $auctions[$key]["current_bid"] = $bids_flag['bid_amount'];
                    }else{
                        $auctions[$key]["current_bid"] = $value['price'];
                    }
                    if(@$request->user_id != ''){
                        $myCurrentBid = Bid::where('ad_id','=',$value['id'])->where('user_id','=',$request->user_id)->orderby('bids.id','DESC')->first();;
                        if($myCurrentBid){
                            $auctions[$key]['my_curent_bid'] = $myCurrentBid;
                        }else{
                            $auctions[$key]['my_curent_bid'] = null;
                        }

                    }

                }

                return $this->dataSuccess('Lấy danh sách đấu giá thành công',$auctions,200);
            }
            catch (\Exception $exception)
            {
                return $this->dataError($exception->getMessage(),null,200);
            }
      }
      public function show(Request $request,$id)
      {
            try{

                $data_return = [];
                $data_return = Ad::select("id","title","description","category_id","price","expired_at","next_min","buy_now")->where('id',$id)->first();

                $diff     = strtotime($data_return->expired_at)-time();
                $data_return->expired_second = $diff;
                $data_return->bid =Bid::where('ad_id','=',$id)->orderby('bids.id','DESC')->get();
                $current = Bid::where('ad_id','=',$id)->orderby('bids.id','DESC')->first();
                if($current){
                    $data_return->current_bid = $current->bid_amount;
                }else{
                  $data_return->current_bid = $data_return->price;
                }

                if(@$request->user_id != ''){
                    $myCurrentBid = Bid::where('ad_id','=',$id)->where('user_id','=',$request->user_id)->orderby('bids.id','DESC')->first();
                    if($myCurrentBid){
                        $data_return->my_curent_bid = $myCurrentBid;
                    }else{
                        $data_return->my_curent_bid = null;
                    }

                }
                return $this->dataSuccess('Lấy đấu giá chi tiết thành công',$data_return,200);

            }
            catch (\Exception $exception)
            {
                return $this->dataError($exception->getMessage(),null,200);
            }
      }

      public function store(Request $request)
      {
          try{
              $auctions = null;
              if($request->price_max > 0 && $request->price_max < $request->price){
                    return $this->dataError("Giá kết thúc phải lớn hơn giá bắt đầu hoặc bằng 0.",[],200);
              }

              $validator = Validator::make($request->all(), [
                      'title' => 'required',
                      'description' => 'required',
                      'category_id' => 'required|numeric',
                      'price' => 'required|numeric',
                      'day_end' => 'required',
                      'step' => 'required|numeric',
                      'price_max' => 'required|numeric'
                  ],[
                    'title.required' => 'Tiêu đề không được để trống',
                    'description.required' => 'Mô tả không được để trống',
                    'category_id.required' => 'Danh mục không được để trống',
                    'price.required' => 'Giá bắt đầu không được để trống',
                    'day_end.required' => 'Số ngày kết thúc không được để trống',
                    'step.required' => 'Giá tiền tối thiểu 1 lượt đấu giá không được để trống',
                    'price_max.required' => 'Giá tiền tối đa đấu giá không được trống không được để trống',
                    'category_id.numeric' => 'Danh mục phải là số',
                    'price.numeric' => 'Giá tiền phải là số',
                    'step.numeric' => 'Giá tiền tối thiểu 1 lượt đấu giá phải là số',
                    'price_max.numeric' => 'Giá tiền tối đa đấu giá Phải là số',
                  ]
              );

              if ($validator->fails()) {
                  return $this->dataError($validator->errors(),null,200);
              }

              if ($request->price > $request->price_max) {
                  return $this->dataError("Giá bắt đầu không được lơn hơn giá tối đa.",null,200);
              }

              $title = $request->title;
              $slug = unique_slug($this->convert_slug($title));
              if($request->day_end == -1){
                  $day  = '+10 min';
              }else{
                  $day  = '+'.$request->day_end.' days';
              }

              $data = [
                  'title'             => $request->title,
                  'slug'              => $slug,
                  'description'       => $request->description,
                  'category_id'       => $request->category_id,
                  'type'              => $request->type,
                  'price'             => $request->price,

                  'seller_name'       => Auth::user()->name,
                  'seller_email'      => Auth::user()->email,
                  'seller_phone'      => Auth::user()->phone,
                  'country_id'        => Auth::user()->country_id,
                  'address'           => Auth::user()->address,

                  'category_type'     => 'auction',
                  'status'            => '1',
                  'user_id'           => Auth::user()->id,
                  'expired_at'        => date("Y-m-d H:i:s", strtotime($day)),
                  'next_min'        => $request->step,
                  'buy_now'        => $request->price_max,
              ];


              $auctions = Ad::create($data);

              if (get_option('ads_moderation') == 'direct_publish'){
                  $data['status'] = '1';
              }

              if ( ! $request->price_plan){
                  $data['price_plan'] = 'regular';
              }


              $this->uploadAdsImage($request,$auctions->id);



              return $this->dataSuccess('Tạo đấu giá thành công',$auctions,200);

          }
          catch (\Exception $exception)
          {
              return $this->dataError($exception->getMessage(),null,200);
          }


      }

      public function uploadAdsImage(Request $request, $ad_id = 0){
          $user_id = 0;

          if (Auth::check()){
              $user_id = Auth::user()->id;
          }

          if ($request->hasFile('images')){
              $images = $request->file('images');
              foreach ($images as $image){
                  $valid_extensions = ['jpg','jpeg','png'];
                  if ( ! in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions) ){
                      return redirect()->back()->withInput($request->input())->with('error', 'Only .jpg, .jpeg and .png is allowed extension') ;
                  }

                  $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
                  $resized = Image::make($image)->resize(640, null, function ($constraint) {
                      $constraint->aspectRatio();
                  })->stream();
                  $resized_thumb = Image::make($image)->resize(320, 213)->stream();

                  $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

                  $imageFileName = 'uploads/images/'.$image_name;
                  $imageThumbName = 'uploads/images/thumbs/'.$image_name;

                  try{
                      //Upload original image
                      $is_uploaded = current_disk()->put($imageFileName, $resized->__toString(), 'public');

                      if ($is_uploaded) {
                          //Save image name into db
                          $created_img_db = Media::create(['user_id' => $user_id, 'ad_id' => $ad_id, 'media_name'=>$image_name, 'type'=>'image', 'storage' => get_option('default_storage'), 'ref'=>'ad']);

                          //upload thumb image
                          current_disk()->put($imageThumbName, $resized_thumb->__toString(), 'public');
                          $img_url = media_url($created_img_db, false);
                      }
                  } catch (\Exception $e){
                      return redirect()->back()->withInput($request->input())->with('error', $e->getMessage()) ;
                  }
              }
          }
      }

      public function getAuctionByUser(Request $request)
      {
          try{
              $auctions = Ad::orderby('id','DESC');
              $dateNow = date("Y-m-d H:i:s");
              if(@$request->status != "")
              {
                  if(@$request->status == "on"){
                      $auctions = $auctions->where('expired_at','>=',$dateNow);
                  }else{
                      $auctions = $auctions->where('expired_at','<',$dateNow);
                  }
              }
              $auctions = $auctions->where('user_id','=',Auth::user()->id);
              $auctions = $auctions->paginate(10);
              foreach ($auctions as $key => $value) {
                  $bids_flag = Bid::where('bids.ad_id','=',$value['id'])->orderby('bids.id','DESC')->first();
                  $count_bid = Bid::where('bids.ad_id','=',$value['id'])->count();
                  $diff     = strtotime($value['expired_at'])-time();
                  $auctions[$key]["expired_second"] = $diff;
                  if($bids_flag){
                      $auctions[$key]["current_bid"] = $bids_flag['bid_amount'];
                  }else{
                      $auctions[$key]["current_bid"] = $value['price'];
                  }
                  $auctions[$key]["count_bid"] = $count_bid;
                  if($value['flag'] == 1){
                      if($count_bid > 0){
                          $auctions[$key]["user_win"] = User::select("name", "email" , "phone" , "address", "photo")->where('id','=',$bids_flag['user_id'])->first();
                      }else{
                          $auctions[$key]["user_win"] = null;
                      }
                  }
              }
              return $this->dataSuccess('Lấy danh sách đấu giá thành công',$auctions,200);

          }
          catch (\Exception $exception)
          {
              return $this->dataError($exception->getMessage(),null,200);
          }


      }
}
