<?php

namespace App\Http\Controllers\API\V1;

use App\Ad;
use App\Bid;
use DB;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class UsersBidControler extends Controller
{



      public function getBidByUsers(Request $request ){
          try {
              $bids = DB::table('bids')
              ->join('ads', 'ads.id', '=', 'bids.ad_id')
              ->orderby('bids.id','DESC');
              if(@$request->user_id != ''){
                $bids = $bids->where('bids.user_id',$request->user_id);
              }
              if(@$request->offset != ''){
                  $bids = $bids->offset($request->offset);
              }
              if(@$request->limit != ''){
                  $bids = $bids->limit($request->limit);
              }
              return $this->dataSuccess('Lấy danh sách đấu giá thành công',$bids->get(),200);
          }
          catch (Exception $e) {
              return $this->dataError($e->getMessage(),null,200);
          }
      }


}
