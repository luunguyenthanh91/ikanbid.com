<?php

namespace App\Http\Controllers\API\V1;

use App\Ad;
use App\Bid;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class UsersAuctionControler extends Controller
{

      // this function call api to skybound and endpoint insiderobjects
      public function getAuctionByUsers(Request $request ){
          try {
              $auctions = Ad::orderby('id','DESC');

              if(@$request->user_id != ''){
                $auctions = $auctions->where('user_id',$request->user_id);
              }
              if(@$request->status == 'active'){
                  $dateNow = date("Y-m-d");
                  $auctions = $auctions->where('expired_at','>=',$dateNow);
              }else if(@$request->status == 'deactive'){
                $dateNow = date("Y-m-d");
                $auctions = $auctions->where('expired_at','<',$dateNow);
              }

              if(@$request->offset != ''){
                  $auctions = $auctions->offset($request->offset);
              }
              if(@$request->limit != ''){
                  $auctions = $auctions->limit($request->limit);
              }

              return $this->dataSuccess('Lấy danh sách đấu giá thành công',$auctions->get(),200);
          }
          catch (Exception $e) {
              return $this->dataError($e->getMessage(),null,200);
          }
      }



}
