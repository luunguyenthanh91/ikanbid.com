@if(get_option('enable_social_login') == 1)

        @if(get_option('enable_facebook_login') == 1)
            <a href="{{ route('facebook_redirect') }}" class="btn btn-social-icon btn-sm btn-facebook"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>

        @endif

        @if(get_option('enable_google_login') == 1)
            <a href="{{ route('google_redirect') }}" class="btn btn-social-icon btn-sm btn-google-plus"><i class="fa fa-google fa-fw" aria-hidden="true"></i></a>

        @endif
        @if(get_option('enable_twitter_login') == 1)
            <a href="{{ route('twitter_redirect') }}" class="btn btn-social-icon btn-sm btn-twitter"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a>

        @endif
@endif
