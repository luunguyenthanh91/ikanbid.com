@extends('ver2.layouts.app')
@section('title') @if( ! empty($title)) {{ strip_tags($title) }} | @endif @parent @endsection

@section('social-meta')
    <meta property="og:title" content="{{ safe_output($ad->title) }}">
    <meta property="og:description" content="{{ substr(trim(preg_replace('/\s\s+/', ' ',strip_tags($ad->description) )),0,160) }}">
    @if($ad->media_img->first())
        <meta property="og:image" content="{{ media_url($ad->media_img->first(), true) }}">
    @else
        <meta property="og:image" content="{{ asset('uploads/placeholder.png') }}">
    @endif
    <meta property="og:url" content="{{  route('single_ad', [$ad->id, $ad->slug]) }}">
    <meta name="twitter:card" content="summary_large_image">
    <!--  Non-Essential, But Recommended -->
    <meta name="og:site_name" content="{{ get_option('site_name') }}">
@endsection
@inject('categorys_tem', 'App\Http\Controllers\TemplateController')
@section('content')
<div id="content">
  <div class="so-page-builder">
    <section id="section_1_h3">
      <div class="container page-builder-ltr">
        <div class="row row-style row_lkda">
          {{$categorys_tem::allCategory()}}
          <!-- // conten -->

          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col_ksde ">

            <div id="content">
              <input type="hidden" class="detail_count" value="{{ $ad->expired_at}}" />
              <div class="sidebar-overlay "></div>
              <div class="product-view product-detail">
                <div class="product-view-inner clearfix">
                   <div class="content-product-left  col-md-5 col-sm-6 col-xs-12">
                    <div class="so-loadeding"></div>
                    <div class="  class-honizol">
                     <div class="box-label">
                      <span class="label-product label-sale" id="current_bid_display">
                          {{number_format($ad->current_bid, 0) }} đ
                      </span>
                     </div>
                     <img class="product-image-zoom" src="{{ $ad->feature_img->media_name }}" data-zoom-image="{{ $ad->feature_img->media_name }}" title="{{safe_output($ad->title)}}" alt="{{safe_output($ad->title)}}">
                    </div>

                   </div>
                   <div class="content-product-right col-md-7 col-sm-6 col-xs-12">
                     @if($ad->flag != 1)
                        <div class="countdown_box">
                         <div class="countdown_inner">
                          <div class="Countdown-1">
                          </div>
                         </div>
                        </div>
                    @endif


                    <div class="title-product">
                     <h1>{{safe_output($ad->title)}}</h1>
                    </div>
                    <div class="box-review">
                      <a class="reviews_button" onclick="">10 lượt xem</a> / <a class="write_review_button" onclick="">Viết Bình Luận</a>
                    </div>

                    <div class="product-box-desc">
                     <div class="inner-box-desc">
                       <div class="widget">

                           @if($ad->category_type== 'jobs')
                               <h3>@lang('app.job_summery')</h3>
                               <p><span class="ad-info-name"><i class="fa fa-houzz"></i> @lang('app.employer')</span> <span class="ad-info-value company-name">{{ $ad->seller_name }}</span></p>
                               <p><span class="ad-info-name"><i class="fa fa-money"></i> @lang('app.salary')</span> <span class="ad-info-value">{!! themeqx_price_ng($ad->price) !!}</span></p>
                               <p><span class="ad-info-name"><i class="fa fa-refresh"></i> @lang('app.mentioned_salary_will_be')</span> <span class="ad-info-value"> @lang('app.'.$ad->job->salary_will_be) </span></p>

                               <p><span class="ad-info-name"><i class="fa fa-newspaper-o"></i> @lang('app.published_on')</span> <span class="ad-info-value">{{ $ad->created_at->format('M d, Y') }}</span></p>
                               <p><span class="ad-info-name"><i class="fa fa-building-o"></i> @lang('app.job_nature')</span> <span class="ad-info-value">@lang('app.'.$ad->job->job_nature)</span></p>
                               <p>
                                   <span class="ad-info-name"><i class="fa fa-map-marker"></i> @lang('app.job_location')</span> <span class="ad-info-value">
                                       @if($ad->job->is_any_where)
                                           @lang('app.any_where_in')
                                       @else
                                           @if($ad->city)
                                               {!! $ad->city->city_name !!},
                                           @endif
                                           @if($ad->state)
                                               {!! $ad->state->state_name !!},
                                           @endif
                                       @endif
                                       {!! $ad->country->country_name !!}
                                   </span>
                               </p>

                               <p><span class="ad-info-name"><i class="fa fa-briefcase"></i> @lang('app.job_validity') : </span> <span class="ad-info-value">@lang('app.'.$ad->job->job_validity)</span></p>


                               <p><span class="ad-info-name"><i class="fa fa-clock-o"></i> @lang('app.application_deadline') : </span> <span class="ad-info-value">{{ date('M d, Y', strtotime($ad->job->application_deadline)) }}</span></p>

                           @else

                               <h3>@lang('app.general_info')</h3>
                               <p><span class="ad-info-name"><i class="fa fa-money"></i> @lang('app.price') : </span> <span class="ad-info-value">{{number_format($ad->price, 0) }} đ</span></p>
                               @if($ad->flag == 1)
                                  <p><span class="ad-info-name"><i class="fa fa-money"></i> Giá thẩu cao nhất : </span> <span class="ad-info-value">{{number_format($ad->current_bid, 0) }} đ</span></p>
                              @endif
                               @if(! empty($ad->country))
                                   <p><span class="ad-info-name"><i class="fa fa-globe"></i>  @lang('app.country') : </span> <span class="ad-info-value"> {!! $ad->country->country_name !!} </span> </p>
                               @endif

                               @if(! empty($ad->state))
                                   <p><span class="ad-info-name"><i class="fa fa-flag-o"></i>  @lang('app.state') : </span> <span class="ad-info-value"> {!! $ad->state->state_name !!} </span> </p>
                               @endif

                               @if(! empty($ad->city))
                                   <p><span class="ad-info-name"><i class="fa fa-area-chart"></i>  @lang('app.city') : </span> <span class="ad-info-value"> {!! $ad->city->city_name !!} </span> </p>
                               @endif

                               <p><span class="ad-info-name"><i class="fa fa-map-marker"></i>  @lang('app.address') : </span> <span class="ad-info-value"> {!! safe_output($ad->address) !!} </span> </p>

                           @endif

                           <p><span class="ad-info-name"><i class="fa fa-calendar-check-o"></i> Ngày Đăng : </span> <span class="ad-info-value">{{$ad->posted_date()}}</span></p>
                           <p><span class="ad-info-name"><i class="fa fa-calendar-check-o"></i> Ngày Hết Hạn : </span> <span class="ad-info-value">{{$ad->expired_date()}}</span></p>



                       </div>
                     </div>
                    </div>



                    <div id="product">
                     <div class="box-cart clearfix">
                          <p id="message_bid"  style="color:red; width : 100%; margin : 10px 0 0 0"></p>

                          <div class="form-group box-info-product">
                              @if($ad->flag != 1)
                               <div class="option quantity">

                                  <div class="input-group quantity-control" unselectable="on" style="user-select: none;padding:0 0px 0 10px">
                                     <input type="number" style="width : 100px" class="form-control" id="bid_amount" name="bid_amount" placeholder="{{$ad->current_bid() + $ad->next_min}}" min="{{$ad->current_bid() + $ad->next_min}}" required="required">
                                     <input type="hidden" value="{{$ad->id}}" id="hidden_id" />

                                  </div>
                               </div>
                               <div class="cart">
                                  <input type="button" id="place_bid_form" value="Đấu Giá" class="addToCart btn btn-mega btn-lg " data-toggle="tooltip" title="" onclick="" data-original-title="Đấu giá">
                               </div>
                               <div class="add-to-links wish_comp">
                                    <ul class="blank">
                                         <li class="wishlist">
                                            <a onclick=""><i class="fa fa-heart"></i></a>
                                         </li>
                                    </ul>
                               </div>
                               @else
                                <div class="alert alert-warning">
                                     <h4>Đã đóng đấu giá</h4>
                                     <p>Bạn không còn có thể đặt bất kỳ giá thầu nào trong phiên đấu giá này</p>
                                 </div>
                               @endif


                          </div>

                          <div class="clearfix"></div>
                          <div class="modern-social-share-btn-group">
                              <h4>Chia Sẻ Đấu Giá Này</h4>
                              <a href="#" class="btn btn-default share s_facebook"><i class="fa fa-facebook"></i> </a>
                              <a href="#" class="btn btn-default share s_plus"><i class="fa fa-google-plus"></i> </a>
                              <a href="#" class="btn btn-default share s_twitter"><i class="fa fa-twitter"></i> </a>
                              <a href="#" class="btn btn-default share s_linkedin"><i class="fa fa-linkedin"></i> </a>
                          </div>
                     </div>
                    </div>
                   </div>
                </div>
               </div>
              <div class="product-attribute module">
                <div class="row content-product-midde clearfix">
                   <div class="col-xs-12">
                    <div class="producttab ">
                     <div class="tabsslider  ">
                      <ul class="nav nav-tabs font-sn">
                         <li class="active"><a data-toggle="tab" href="#tab-description">Thông Tin Chi Tiết</a></li>
                         <li><a href="#tab-review" data-toggle="tab">Danh Sách Đã Đấu Giá</a></li>
                      </ul>
                      <div class="tab-content ">
                         <div class="tab-pane active" id="tab-description">
                           <div class="ads-detail">
                               {!! nl2br(safe_output($ad->description)) !!}
                           </div>
                         </div>
                         <div class="tab-pane" id="tab-review">
                           @if($ad->category_type == 'auction')
                               <hr />
                               <div id="bid_history">

                                   @if($ad->bids->count())
                                       <table class="table table-striped">
                                          <thead>
                                               <tr>
                                                   <th>@lang('app.bidder')</th>
                                                   <th>@lang('app.bid_amount')</th>
                                                   <th>@lang('app.date_time')</th>
                                               </tr>
                                           </thead>
                                           <tbody id="list_bids">
                                           @foreach($ad->bids as $bid)
                                               <tr>
                                                   <td style="text-align: center;">@lang('app.bidder') #{{$bid->user_id}}</td>
                                                   <td style="text-align: center;">{{number_format($bid->bid_amount, 0) }} đ</td>
                                                   <td style="text-align: center;">{{$bid->posting_datetime() }}</td>
                                               </tr>
                                           @endforeach
                                         </tbody>

                                       </table>
                                   @else
                                   <table class="table table-striped">
                                      <thead>
                                           <tr>
                                               <th>@lang('app.bidder')</th>
                                               <th>@lang('app.bid_amount')</th>
                                               <th>@lang('app.date_time')</th>
                                           </tr>
                                       </thead>
                                       <tbody id="list_bids">
                                          <tr>
                                              <td colspan="3"><p>@lang('app.there_is_no_bids')</p></td>
                                          </tr>
                                        </tbody>

                                      </table>
                                   @endif
                               </div>
                           @endif
                         </div>


                      </div>
                     </div>
                    </div>
                   </div>
                </div>
              </div>



          </div>



        </div>
      </div>
    </section>
  </div>
</div>

@endsection

@section('page-js')
    <script>
        const formatter = new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          minimumFractionDigits: 0
        });
        $(function(){
            var curent_bid_auction = parseFloat({{$ad->current_bid()}} + '');
            var bid_next = parseFloat({{$ad->next_min}} + '');
            $('#place_bid_form').click(function(){

                @if (Auth::guest())
                      $("#message_bid").html("Vui lòng đăng nhập để đấu giá.");

                @elseif (Auth::user()->id == $ad->user_id)
                    $("#message_bid").html("Bạn không thể tự đấu giá cho chính bạn.");
                @else
                    var bid_this = parseFloat($("#bid_amount").val());
                    if(bid_next > 0){
                        if(bid_this >= curent_bid_auction+bid_next ){
                            $.ajax({
                                type : 'POST',
                                url : '/{{$ad->id}}/post-bid',
                                data : { bid_amount :bid_this ,_token : '{{ csrf_token() }}' },
                                success : function (data) {
                                  if(data['result'] == false){
                                      $("#message_bid").html(data['message']);
                                  }else{
                                      $("#message_bid").html("Bạn đã đấu giá thành công.");
                                      $("#bid_amount").val("");
                                      curent_bid_auction = bid_this;
                                      var data_emit = {'success' : data['data']['is_accepted'] ,"user_name" : "{{Auth::user()->id}}" ,'create_at' : data['data']['create_ad'] , "id_auction":{{$ad->id}}+"", "current_bid":bid_this , "current_bid_display" :data['data']['current_bid_display'] };
                                      socket.emit('realtime_bid', JSON.stringify(data_emit));
                                      socket.emit('notify_return', JSON.stringify(data['data']['data_message']));
                                  }

                                }
                            });
                        }else{
                            $("#message_bid").html("Lượt bid tiếp theo tối thiểu phải là : "+(formatter.format(curent_bid_auction+bid_next)));
                        }
                    }else{
                        if(bid_this >= curent_bid_auction+1 ){
                          $.ajax({
                              type : 'POST',
                              url : '/{{$ad->id}}/post-bid',
                              data : { bid_amount :bid_this ,_token : '{{ csrf_token() }}' },
                              success : function (data) {
                                if(data['result'] == false){
                                    $("#message_bid").html(data['message']);
                                }else{

                                    $("#message_bid").html("Bạn đã đấu giá thành công.");
                                    $("#bid_amount").val("");
                                    curent_bid_auction = bid_this;
                                    var data_emit = {'success' : data['data']['is_accepted'] ,"user_name" : "{{Auth::user()->id}}"  ,'create_at' : data['data']['create_ad'], "id_auction":{{$ad->id}}+"", "current_bid":bid_this , "current_bid_display" : data['data']['current_bid_display'] };
                                    socket.emit('realtime_bid', JSON.stringify(data_emit));
                                    socket.emit('notify_return', JSON.stringify(data['data']['data_message']));
                                  }
                              }
                          });
                        }else{
                              $("#message_bid").html("Lượt bid tiếp theo tối thiểu phải là : " + (formatter.format(curent_bid_auction+1)));
                        }
                    }

                @endif

            });







        });
    </script>

    <script>
        $(function(){
            // var id_auction = {{$ad->id}}+'';
            // var current_bid = {{$ad->current_bid()}} + '';
            // var current_bid_display = '{!! themeqx_price($ad->current_bid()) !!}' + '';
            // if(id_auction != '' && current_bid != ''){
            //     var data_emit = { "id_auction":id_auction, "current_bid":current_bid , "current_bid_display" : current_bid_display};
            //     socket.emit('realtime_bid', JSON.stringify(data_emit));
            // }
        });

        


        socket.on("sever_update_bid",function(data_client_on){
            var bid_next = parseFloat({{$ad->next_min}} + '');
            var data_reponse = JSON.parse(data_client_on);
            var curent_id_hidden = $("#hidden_id").val();
            if(data_reponse.id_auction == curent_id_hidden){
                curent_bid_auction = data_reponse.current_bid;
                $("#current_bid_display").html(data_reponse.current_bid_display);
                if(parseInt(data_reponse.success) == 1){
                    location.reload();
                }else{
                    if(bid_next > 0){
                        $("#bid_amount").attr("min",parseFloat(data_reponse.current_bid)+bid_next);
                        $("#bid_amount").attr("placeholder",parseFloat(data_reponse.current_bid)+bid_next);
                    }else{
                        $("#bid_amount").attr("min",parseFloat(data_reponse.current_bid)+1);
                        $("#bid_amount").attr("placeholder",parseFloat(data_reponse.current_bid)+1);

                    }
                    html  = '<tr>';
                    html  +=        '<td style="text-align: center;"> Nhà thầu #'+data_reponse.user_name+'</td>';
                    html  +=        '<td style="text-align: center;">'+data_reponse.current_bid_display+'</td>';
                    html  +=        '<td style="text-align: center;">'+data_reponse.create_at+'</td>';
                    html  +=      '</tr>';
                    $('#list_bids').html(html+$('#list_bids').html());
                    $(".home_current_bid_"+data_reponse.id_auction).html(data_reponse.current_bid_display);
                }


            }
        });

    </script>
    <!-- <script>

        socket.on("sever_update_bid",function(data_client_on){
            var data_reponse = JSON.parse(data_client_on);
            $("#home_current_bid_"+data_reponse.id_auction).html(data_reponse.current_bid_display);
        });

    </script> -->
@endsection
