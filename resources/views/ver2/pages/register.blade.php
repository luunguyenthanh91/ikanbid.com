@extends('ver2.layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@inject('categorys_tem', 'App\Http\Controllers\TemplateController')
@section('content')
<div id="content">
  <div class="so-page-builder">
    <section id="section_1_h3">
      <div class="container page-builder-ltr">
        <div class="row row-style row_lkda">

          <div class="main-container container">
            <ul class="breadcrumb">
              <li><a href="/"><i class="fa fa-home"></i></a></li>
              <li><a href="/register">Đăng ký</a></li>
            </ul>

            <div class="row">
              <div id="content" class="col-md-12">
                <div class="col-sm-6">
                  <div class="well ">
                    <h2>Đăng nhập</h2>
                    <p><strong>Bạn đã là thành viên của Ikanbid?</strong></p>
                    <p>Hãy nhanh chống đăng nhập để đừng bỏ lỡ những sản phẩm với giá mong đợi</p>
                    <a href="{{ route('login') }}" class="btn btn-primary">Tiếp tục</a>
                  </div>
                </div>
                <div class="col-sm-6">
                    <div class="well ">
                        <h2 class="title">Thành Viên Mới</h2>
                        <p>Trở thành thành viên của Ikanbid để được thoả thích mua sắm với những sản phẩm siêu to siêu rẻ.</p>
                        <form role="form" method="POST" action="" class="form-horizontal account-register clearfix">
                          {{ csrf_field() }}
                          <fieldset id="account">
                            <legend>Thông Tin Chung</legend>
                            <div class="form-group required" style="display: none;">
                              <label class="col-sm-2 control-label">Customer Group</label>
                              <div class="col-sm-10">
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="customer_group_id" value="1" checked="checked"> Default
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="form-group required">
                              <label class="col-sm-2 control-label" for="input-firstname">Tên Tài Khoản</label>
                              <div class="col-sm-10">
                                <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus class="form-control">
                              </div>
                            </div>

                            <div class="form-group required">
                              <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
                              <div class="col-sm-10">
                                <input type="email" name="email" value="{{ old('email') }}" required class="form-control">
                                @if ($errors->has('email'))
                                    <span style="color : red" class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                              </div>
                            </div>

                          </fieldset>
                          <fieldset>

                            <legend>Mật Khẩu Của Bạn</legend>
                            <div class="form-group required">
                              <label class="col-sm-2 control-label" for="input-password">Mật Khẩu</label>
                              <div class="col-sm-10">
                                <input type="password" type="password" name="password" required placeholder="Mật Khẩu" id="input-password" class="form-control">
                              </div>
                            </div>
                            <div class="form-group required">
                              <label class="col-sm-2 control-label" for="input-confirm">Nhập Lại Mật Khẩu</label>
                              <div class="col-sm-10">
                                <input type="password" name="password_confirmation" required placeholder="Nhập Lại Mật Khẩu" id="input-confirm" class="form-control">
                              </div>
                            </div>

                          </fieldset>

                          <div class="buttons">
                            <div class="pull-right">
                              <input type="submit" value="Tiếp Tục" class="btn btn-primary">
                            </div>
                            @include('auth.social_login')
                          </div>
                        </form>
                    </div>
                </div>
              </div>

            </div>
          </div>


        </div>
      </div>
    </section>
  </div>
</div>
@endsection
