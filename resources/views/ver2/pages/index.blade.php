@extends('ver2.layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@inject('categorys_tem', 'App\Http\Controllers\TemplateController')
@section('content')
<div id="content">
  <div class="so-page-builder">
    <section id="section_1_h3">
      <div class="container page-builder-ltr">
        <div class="row row-style row_lkda">
            {{$categorys_tem::allCategory()}}
          <!-- // conten -->

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col_8sje slider_container">
                  <div class="row row_mwsi row-style">
                    <!-- //banner -->
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 col_5q4o slider-layout-3">
                      <div class="module sohomepage-slider so-homeslider-ltr">
                        <div class="modcontent">

                          <div id="sohomepage-slider1">
                            <div class="so-homeslider yt-content-slider full_slider owl-drag" data-rtl="yes" data-autoplay="yes" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="10" data-items_column00="1" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="yes" data-lazyload="yes" data-loop="yes" data-hoverpause="yes">
                              <div class="item">
                                 <a href=" #   " title="slide 1 - 1" target="_self">
                                 <img class="responsive" src="{{ asset('assets/ver2/image/bg_1.png') }}" alt="slide 1 - 1">
                                 </a>
                                 <div class="sohomeslider-description">
                                 </div>
                              </div>
                              <div class="item">
                                 <a href=" #   " title="slide 1 - 2" target="_self">
                                 <img class="responsive" src="{{ asset('assets/ver2/image/bg_2.png') }}" alt="slide 1 - 2">
                                 </a>
                                 <div class="sohomeslider-description">
                                 </div>
                              </div>


                           </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- //banner -->


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_8jd3 deal-layout-3 hidden-sm hidden-md hidden-xs">
                      <div class="module so-deals-ltr home3_deal">
                        <div class="modcontent">


                          <div id="so_deals_938" class="so-deal banner modcontent products-list grid clearfix preset00-1 preset01-1 preset02-1 preset03-1 preset04-1  button-type1  style2">

                            <div class="extraslider-inner" data-effect="none">
                              @foreach($product_hot as $key => $top_product)

                              <div class="item">
                                <div class="transition product-layout">
                                  <div class="product-item-container ">
                                    <div class="left-block so-quickview">
                                      <div class="box-label">
                                        <span class="label-product label-sale home_current_bid_{{$top_product->id}}">{{number_format($top_product->current_bid, 0) }} VNĐ</span>
                                      </div>
                                      <div class="product-image-container">
                                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self">
                                        <img src="{{ $top_product->feature_img->media_name }}" alt="{{$top_product->title}}" class="img-responsive">
                                        </a>
                                      </div>
                                    </div>
                                    <div class="right-block">
                                      <div class="caption">
                                        <div class="item-time">
                                          <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                          <input type="hidden" class="timer_val" value="{{$top_product->expired_at}}">
                                          <div class="item-timer product_hot_time prouct_hot_{{$top_product->id}}" id="prouct_hot_{{$top_product->id}}"></div>
                                        </div>
                                        <h4><a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self" title="{{$top_product->title}}"> {{$top_product->title}}</a></h4>

                                      </div>
                                      <div class="button-group2">
                                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}"  class="bt-cart addToCart" type="button" data-toggle="tooltip" title="Add to Cart" > <span>Đấu giá</span></a>
                                        <button class="bt wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('{{$top_product->id}}');"><i class="fa fa-heart"></i></button>
                                        <!-- <button class="bt compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              @endforeach

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_stzj block">
                      <div class="cate-html">
                        <ul class="cate-html-item contentslider" data-rtl="no" data-loop="no" data-autoplay="yes" data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6" data-margin="27" data-items_column0="5" data-items_column1="3" data-items_column2="3" data-items_column3="3" data-items_column4="2" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-hoverpause="yes">

                          @foreach($top_categories as $key => $top_cat)
                          <li class="item">
                            <div class="item-image"><a title="{{$top_cat->category_name}}" href="{{ route('search', [ 'category' => $top_cat->id.'-'.$top_cat->category_slug]) }}"><img src="{{$top_cat->image}}" alt="{{$top_cat->category_name}}"></a></div>
                            <div class="item-content">
                            <h4><a href="{{ route('search', [ 'category' => $top_cat->id.'-'.$top_cat->category_slug]) }}">{{$top_cat->category_name}}</a></h4>
                            </div>
                          </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_ksde block">

                      <div class="module so-extraslider-ltr">
      									<h3 class="modtitle title-category ">Sản phẩm hot nhất</h3>

      									<div class="modcontent module so-deals-ltr home3_deal" >
      										<div id="so_extra_slider_528" class="so-deal modcontent products-list grid preset00-4 preset01-4 preset02-2 preset03-2 preset04-2 button-type1">

                            <div class="box-banner">
      												<div class="banners">
      												</div>
      											</div>
      											<!-- Begin extraslider-inner -->
      											<div class="extraslider-inner products-list" data-effect="none">

                              @foreach($product_top as $key => $top_product)
                              <div class="item">
                                <div class="transition product-layout">
                                  <div class="product-item-container ">
                                    <div class="left-block so-quickview">
                                      <div class="box-label">
                                        <span class="label-product label-sale home_current_bid_{{$top_product->id}}" >{{number_format($top_product->current_bid, 0) }} VNĐ</span>
                                      </div>
                                      <div class="product-image-container">
                                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self">
                                        <img src="{{ $top_product->feature_img->media_name }}" alt="{{$top_product->title}}" class="img-responsive">
                                        </a>
                                      </div>
                                    </div>
                                    <div class="right-block">
                                      <div class="caption">
                                        <div class="item-time">
                                          <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                          <input type="hidden" class="timer_val" value="{{$top_product->expired_at}}">
                                          <div class="item-timer product_hot_time prouct_hot_{{$top_product->id}}" id="prouct_hot_{{$top_product->id}}"></div>
                                        </div>
                                        <h4><a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self" title="{{$top_product->title}}"> {{$top_product->title}}</a></h4>

                                      </div>
                                      <div class="button-group2">
                                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}"  class="bt-cart addToCart" type="button" data-toggle="tooltip" title="Add to Cart" > <span>Đấu giá</span></a>
                                        <button class="bt wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('{{$top_product->id}}');"><i class="fa fa-heart"></i></button>
                                        <!-- <button class="bt compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              @endforeach

      											</div>
      										</div>


      									</div>
      								</div>
      							</div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_kjmz block">
                      <div class="banner-21 banner">
                        <div>
                          <a class="bn-shadow" href="#" title="Banner 24">
                          <img src="https://hackernoon.com/hn-images/1*lEyU2zS2TBaOl_fbXqPChA.png" alt="Static Image">
                          </a>
                        </div>
                      </div>
                    </div>

                    @foreach($product_categories as  $item)
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_ksde block">

                          <div class="module so-extraslider-ltr">
          									<a href="{{ route('search', [ 'category' => $item->id.'-'.$item->category_slug]) }}"><h3 class="modtitle title-category ">{{$item->category_name}}</h3></a>

          									<div class="modcontent module so-deals-ltr home3_deal" >
          										<div  class="so_extra_slider_play modcontent products-list grid preset00-4 preset01-4 preset02-2 preset03-2 preset04-2 button-type1">

                                <div class="box-banner">
          												<div class="banners">
          												</div>
          											</div>
          											<!-- Begin extraslider-inner -->
          											<div class="extraslider-inner products-list" data-effect="none">

                                  @foreach($item->product_top as $key => $top_product)
                                  <div class="item  m-0 mb-1 p-0 col-lg-2 col-md-4 col-sm-6 col-xs-6">
                                    <div class="transition product-layout">
                                      <div class="product-item-container ">
                                        <div class="left-block so-quickview">
                                          <div class="box-label">
                                            <span class="label-product label-sale home_current_bid_{{$top_product->id}}">{{number_format($top_product->current_bid, 0) }} VNĐ</span>
                                          </div>
                                          <div class="product-image-container">
                                            <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self">
                                            <img src="{{ $top_product->feature_img->media_name }}" alt="{{$top_product->title}}" class="img-responsive">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="right-block">
                                          <div class="caption">
                                            <div class="item-time">
                                              <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                              <input type="hidden" class="timer_val" value="{{$top_product->expired_at}}">
                                              <div class="item-timer product_hot_time prouct_hot_{{$top_product->id}}" id="prouct_hot_{{$top_product->id}}"></div>
                                            </div>
                                            <h4><a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self" title="{{$top_product->title}}"> {{$top_product->title}}</a></h4>

                                          </div>
                                          <div class="button-group2">
                                            <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}"  class="bt-cart addToCart" type="button" data-toggle="tooltip" title="Add to Cart" > <span>Đấu giá</span></a>
                                            <button class="bt wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('{{$top_product->id}}');"><i class="fa fa-heart"></i></button>
                                            <!-- <button class="bt compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button> -->
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  @endforeach


          											</div>
          										</div>


          									</div>
          								</div>
          							</div>
                    @endforeach




                  </div>
                </div>
        </div>
      </div>
    </section>
  </div>
</div>
@endsection


@section('page-js')


    <script>

        socket.on("sever_update_bid",function(data_client_on){
            var data_reponse = JSON.parse(data_client_on);
            $(".home_current_bid_"+data_reponse.id_auction).html(data_reponse.current_bid_display);
        });

    </script>
@endsection
