@extends('ver2.layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@inject('categorys_tem', 'App\Http\Controllers\TemplateController')
@section('content')
<div id="content">
  <div class="so-page-builder">
    <section id="section_1_h3">
      <div class="container page-builder-ltr">
        <div class="row row-style row_lkda">
          {{$categorys_tem::allCategory()}}
          <!-- // conten -->

          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col_ksde block">

            <div class="module so-extraslider-ltr">
              <h3 class="modtitle title-category ">{{$title}}</h3>

              <div class="modcontent module so-deals-ltr home3_deal" >
                <div  class="so_extra_slider_play modcontent products-list grid preset00-4 preset01-4 preset02-2 preset03-2 preset04-2 button-type1">

                  <div class="box-banner">
                    <div class="banners">
                    </div>
                  </div>
                  <!-- Begin extraslider-inner -->
                  <div class="extraslider-inner products-list" data-effect="none">
                    @if(!$ads->isEmpty())
                        @foreach($ads as $key => $top_product)
                        <div class="item  m-0 mb-1 p-0  col-lg-2 col-md-3 col-sm-6 col-xs-6">
                          <div class="transition product-layout">
                            <div class="product-item-container ">
                              <div class="left-block so-quickview">
                                <div class="box-label">
                                  <span class="label-product label-sale home_current_bid_{{$top_product->id}}">{{number_format($top_product->current_bid, 0) }} VNĐ</span>
                                </div>
                                <div class="product-image-container">
                                  <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self">
                                  <img src="{{ $top_product->feature_img->media_name }}" alt="{{$top_product->title}}" class="img-responsive">
                                  </a>
                                </div>
                              </div>
                              <div class="right-block">
                                <div class="caption">
                                  <div class="item-time">
                                    <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                    <input type="hidden" class="timer_val" value="{{$top_product->expired_at}}">
                                    <div class="item-timer product_hot_time prouct_hot_{{$top_product->id}}" id="prouct_hot_{{$top_product->id}}"></div>
                                  </div>
                                  <h4><a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self" title="{{$top_product->title}}"> {{$top_product->title}}</a></h4>

                                </div>
                                <div class="button-group2">
                                  <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}"  class="bt-cart addToCart" type="button" data-toggle="tooltip" title="Add to Cart" > <span>Đấu giá</span></a>
                                  <button class="bt wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('{{$top_product->id}}');"><i class="fa fa-heart"></i></button>
                                  <!-- <button class="bt compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button> -->
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                    @else
                      <div class="alert alert-warning" role="alert">
                          Không có sản phẩm với điều kiện này.
                      </div>
                    @endif

                  </div>

                </div>


              </div>
              {{$ads}}
            </div>

          </div>



        </div>
      </div>
    </section>
  </div>
</div>
@endsection


@section('page-js')


    <script>

        socket.on("sever_update_bid",function(data_client_on){
            var data_reponse = JSON.parse(data_client_on);
            $(".home_current_bid_"+data_reponse.id_auction).html(data_reponse.current_bid_display);
        });

    </script>
@endsection
