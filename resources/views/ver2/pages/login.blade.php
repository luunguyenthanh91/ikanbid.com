@extends('ver2.layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@inject('categorys_tem', 'App\Http\Controllers\TemplateController')
@section('content')
<div id="content">
  <div class="so-page-builder">
    <section id="section_1_h3">
      <div class="container page-builder-ltr">
        <div class="row row-style row_lkda">

          <div class="container">
            <div class="row">
                <ul class="breadcrumb">
                  <li><a href="/"><i class="fa fa-home"></i></a></li>
                  <li><a href="/login">Đăng Nhập</a></li>
                </ul>
              <div id="content" class="col-md-12">
                <div class="row">
                    <div class="col-sm-6">
                      <div class="well ">
                        <h2>Tài Khoản Mới</h2>
                        <p><strong>Đăng Ký Tài Khoản</strong></p>
                        <p>Hãy nhanh tay đăng ký thành viên mới để việc mua hàng đấu giá không gián đoạn . Ikanbid đem đến quý khách có những trải nghiệm mới về người dùng.</p>
                        <a href="{{ route('register') }}" class="btn btn-primary">Tiếp tục</a></div>
                    </div>
                    <div class="col-sm-6">

                        <div class="well col-sm-12">

                            <h2>Đăng nhập</h2>
                            <p><strong>Bạn là thành viên Ikanbid ?</strong></p>
                            <form class="form-horizontal" role="form" method="POST" action="">
                              {{ csrf_field() }}
                              <div class="">
                                <label class="control-label" for="input-email">E-Mail Address</label>
                                <input  id="email" type="email" name="email" value="{{ old('email') }}" required autofocus class="form-control">
                                @if ($errors->has('email'))
                                <span style="color:red" class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                              </div>
                              <div class="">
                                <label class="control-label" for="input-password">Mật khẩu</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                <span style="color:red"  class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                                <a href="{{ route('password.request') }}">Forgotten Password</a></div>

                                <input type="submit" value="Đăng Nhập" class="btn btn-primary pull-left">
                            </form>
                            <column id="column-login" class="col-sm-8 pull-right">
                              <div class="row">
                                  <div class="social_login pull-right" id="so_sociallogin">
                                    @include('auth.social_login')
                                  </div>
                              </div>
                            </column>
                      </div>
                    </div>
                  </div>
              </div>

            </div>

          </div>


        </div>
      </div>
    </section>
  </div>
</div>
@endsection
