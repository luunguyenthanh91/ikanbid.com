



<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col_iksd menu_vertical">
  <div class="row row-style row-lkdn">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_kkwm col-style megamenu-style-dev">
      <div class="responsive">
        <div class="so-vertical-menu no-gutter">

          <nav class="navbar-default">
            <div class=" container-megamenu  container   vertical  ">
              <div id="menuHeading">
                <div class="megamenuToogle-wrapper">
                  <div class="megamenuToogle-pattern">
                    <div class="container">
                      <div><span></span><span></span><span></span></div>
                      <span class="title-mega">
                          Danh Mục Đấu Giá
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="navbar-header">
                <span class="title-navbar hidden-lg hidden-md">  Danh Mục Đấu Giá  </span>
                <button type="button" id="show-verticalmenu" data-toggle="collapse" class="navbar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
              </div>
              <div class="vertical-wrapper">
                <span id="remove-verticalmenu" class="fa fa-times"></span>
                <div class="megamenu-pattern">
                  <div class="container">
                    <ul class="megamenu" data-transition="slide" data-animationtime="300">
                      @if($top_categories->count())

                              @foreach($top_categories as $key => $top_cat)
                                  <p style="display : none">{{$key = $key +1}}</p>
                                  <li class="item-vertical  style1">
                                      <p class='close-menu'></p>
                                      <a href="{{ route('search', [ 'category' => $top_cat->id.'-'.$top_cat->category_slug]) }}" class="clearfix">
                                      <span>
                                      <strong><i class="icon"></i><img width="15px" src="{{$top_cat->icon}}" alt="">{{$top_cat->category_name}}</strong>
                                      </span>
                                      </a>
                                  </li>
                              @endforeach
                      @endif

                      <!-- <li class="loadmore"><i class="fa fa-plus-square"></i><span class="more-view"> Xem thêm</span></li> -->
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>

        </div>
      </div>
    </div>



    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  col_d2sm block">
      <div class="module so-extraslider-ltr home3_extra_style2 bn-shadow home3_deal">
        <h3 class="modtitle">Quan Tâm Nhất</h3>
        <div class="modcontent">
          <div id="so_extra_slider_305" class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-2 preset04-1 button-type1">
            <div class="box-banner">
              <div class="banners">
              </div>
            </div>
            <!-- Begin extraslider-inner -->
            <div class="extraslider-inner products-list grid" data-effect="none">
              @foreach($product_view as $key => $top_product)
              <div class="item">
                <div class="transition product-layout">
                  <div class="product-item-container ">
                    <div class="left-block so-quickview">
                      <div class="box-label">
                        <span class="label-product label-sale home_current_bid_{{$top_product->id}}" >{{number_format($top_product->current_bid, 0) }} đ</span>
                      </div>
                      <div class="product-image-container">
                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self">
                        <img src="{{ $top_product->feature_img->media_name }}" alt="{{$top_product->title}}" class="img-responsive">
                        </a>
                      </div>
                    </div>
                    <div class="right-block">
                      <div class="caption">
                        <div class="item-time">
                          <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                          <input type="hidden" class="timer_val" value="{{$top_product->expired_at}}">
                          <div class="item-timer product_hot_time prouct_hot_{{$top_product->id}}" id="prouct_hot_{{$top_product->id}}"></div>
                        </div>
                        <h4><a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self" title="{{$top_product->title}}"> {{$top_product->title}}</a></h4>

                      </div>
                      <div class="button-group2">
                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}"  class="bt-cart addToCart" type="button" data-toggle="tooltip" title="Add to Cart" > <span>Đấu giá</span></a>
                        <button class="bt wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('{{$top_product->id}}');"><i class="fa fa-heart"></i></button>
                        <!-- <button class="bt compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach

            </div>
            <!--End extraslider-inner -->
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_d2sm block">
      <div class="module so-extraslider-ltr home3_extra_style2 bn-shadow home3_deal">
        <h3 class="modtitle">Vừa Xem</h3>
        <div class="modcontent">
          <div id="so_extra_slider_306" class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-2 preset04-1 button-type1">
            <div class="box-banner">
              <div class="banners">
              </div>
            </div>
            <!-- Begin extraslider-inner -->
            <div class="extraslider-inner products-list grid" data-effect="none">
              @foreach($product_view as $key => $top_product)
              <div class="item">
                <div class="transition product-layout">
                  <div class="product-item-container ">
                    <div class="left-block so-quickview">
                      <div class="box-label">
                        <span class="label-product label-sale home_current_bid_{{$top_product->id}}" >{{number_format($top_product->current_bid, 0) }} đ</span>
                      </div>
                      <div class="product-image-container">
                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self">
                        <img src="{{ $top_product->feature_img->media_name }}" alt="{{$top_product->title}}" class="img-responsive">
                        </a>
                      </div>
                    </div>
                    <div class="right-block">
                      <div class="caption">
                        <div class="item-time">
                          <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                          <input type="hidden" class="timer_val" value="{{$top_product->expired_at}}">
                          <div class="item-timer product_hot_time prouct_hot_{{$top_product->id}}" id="prouct_hot_{{$top_product->id}}"></div>
                        </div>
                        <h4><a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}" target="_self" title="{{$top_product->title}}"> {{$top_product->title}}</a></h4>

                      </div>
                      <div class="button-group2">
                        <a href="{{ route('single_ad', [$top_product->id, $top_product->slug]) }}"  class="bt-cart addToCart" type="button" data-toggle="tooltip" title="Add to Cart" > <span>Đấu giá</span></a>
                        <button class="bt wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('{{$top_product->id}}');"><i class="fa fa-heart"></i></button>
                        <!-- <button class="bt compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach

            </div>
            <!--End extraslider-inner -->
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_dmsm block">
      <div class="block-testimonial bn-shadow">
        <div class="testimonial-items contentslider" data-rtl="no" data-loop="no" data-autoplay="yes" data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1" data-items_column2="1" data-items_column3="1" data-items_column4="1" data-arrows="no" data-pagination="yes" data-lazyload="yes" data-hoverpause="yes">
          <div class="item">
            <div class="text">
              <div class="t">Chúng tôi chuyên về những sản phẩm xe hơi cũ , đồ điện máy cũ . Mang đến những trãi nghiệm đồ dùng cũ và chất lượng đến với người dùng Việt Nam</div>
            </div>
            <div class="img"><img src="{{ asset('assets/ver2/image/catalog/demo/banners/home3/user-2.jpg') }}" alt="Static Image"></div>
            <div class="name">Sharon Stone</div>
            <div class="job">Acc - Hollywood</div>
          </div>
          <div class="item">
            <div class="text">
              <div class="t">
                Chúng tôi chuyên về những sản phẩm xe hơi cũ , đồ điện máy cũ . Mang đến những trãi nghiệm đồ dùng cũ và chất lượng đến với người dùng Việt Nam
              </div>
            </div>
            <div class="img"><img src="{{ asset('assets/ver2/image/catalog/demo/banners/home3/user-1.jpg') }}" alt="Static Image"></div>
            <div class="name">David Beckham</div>
            <div class="job">CE0 - Magentech</div>
          </div>
          <div class="item">
            <div class="text">
              <div class="t">
                Chúng tôi chuyên về những sản phẩm xe hơi cũ , đồ điện máy cũ . Mang đến những trãi nghiệm đồ dùng cũ và chất lượng đến với người dùng Việt Nam
              </div>
            </div>
            <div class="img"><img src="{{ asset('assets/ver2/image/catalog/demo/banners/home3/user-3.jpg') }}" alt="Static Image"></div>
            <div class="name">Johny Walker</div>
            <div class="job">Manager - United</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
