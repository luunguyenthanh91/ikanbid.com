<!DOCTYPE html>
<html lang="en">

<head>
		<!-- Basic page needs
				 ============================================ -->
		<meta charset="utf-8">
		<meta name="keywords" content="html5 template, best html5 template, best html template, html5 basic template, multipurpose html5 template, multipurpose html template, creative html templates, creative html5 templates" />
		<meta name="description" content="SuperMarket is a powerful Multi-purpose HTML5 Template with clean and user friendly design. It is definite a great starter for any eCommerce web project." />
		<meta name="author" content="Magentech">
		<meta name="robots" content="index, follow" />
    <title>@section('title') {{ get_option('site_title') }} @show</title>
		<!-- Mobile specific metas
				 ============================================ -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Favicon
				 ============================================ -->
		<link rel="shortcut icon" type="image/png" href="{{ asset('assets/ver2/ico/favicon-16x16.png') }}" />
		<!-- Libs CSS
				 ============================================ -->
		<link rel="stylesheet" href="{{ asset('assets/ver2/css/bootstrap/css/bootstrap.min.css') }}">
		<link href="{{ asset('assets/ver2/css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/js/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/js/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/themecss/lib.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/js/minicolors/miniColors.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/js/slick-slider/slick.css') }}" rel="stylesheet">
		<!-- Theme CSS
				 ============================================ -->
		<link href="{{ asset('assets/ver2/css/themecss/so_searchpro.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/themecss/so_megamenu.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/themecss/so-listing-tabs.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/themecss/so-newletter-popup.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/footer/footer3.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/header/header3.css') }}" rel="stylesheet">
		<link id="color_scheme" href="{{ asset('assets/ver2/css/home3.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/responsive.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/ver2/css/quickview/quickview.css') }}" rel="stylesheet">
		<!-- Google web fonts
				 ============================================ -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i' rel='stylesheet' type='text/css'>
		<style type="text/css">
				body {
						font-family: Roboto, sans-serif;
				}
		</style>

		<script type="text/javascript" src="{{ asset('assets/ver2/js/jquery-2.2.4.min.js') }}"></script>
		<script src="https://ikanbid.kennjdemo.com:4322/socket.io/socket.io.js"></script>
		<script src="{{ asset('assets/js/realtime_bid.js') }}"></script>

    @yield('page-css')
</head>

<body class="common-home ltr layout-3">
	<div id="wrapper" class="wrapper-full banners-effect-10">
		<!-- Header Container  -->
		@include('ver2.component.header')
		<!-- //Header Container  -->
		<!-- Main Container  -->
    @yield('content')

		<!-- //Main Container -->
		<!-- Footer Container -->

    @include('ver2.component.footer')

		</div>

		<div class="back-to-top"><i class="fa fa-angle-up"></i></div>
		<!-- //end Footer Container -->

	<!-- End Color Scheme
			 ============================================ -->
	<!-- Include Libs & Plugins
			 ============================================ -->
	<!-- Placed at the end of the document so the pages load faster -->

	<script type="text/javascript" src="{{ asset('assets/ver2/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/themejs/so_megamenu.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/owl-carousel/owl.carousel.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/slick-slider/slick.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/themejs/libs.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/unveil/jquery.unveil.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/countdown/jquery.countdown.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/datetimepicker/moment.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/jquery-ui/jquery-ui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/modernizr/modernizr-2.6.2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/minicolors/jquery.miniColors.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/jquery.nav.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/quickview/jquery.magnific-popup.min.js') }}"></script>
	<!-- Theme files
			 ============================================ -->
	<script type="text/javascript" src="{{ asset('assets/ver2/js/themejs/application.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/themejs/homepage.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/themejs/custom_h3.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/ver2/js/themejs/addtocart.js') }}"></script>
  @yield('page-js')
</body>

</html>
