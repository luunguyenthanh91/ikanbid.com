
<header id="header" class=" typeheader-3">
  <!-- Header Top -->
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-xs-4 col-md-9 col-lg-6">
          <div class="megamenu-style-dev megamenu-dev">
            <div class="responsive">
              <nav class="navbar-default">
                <div class=" container-megamenu   horizontal ">
                  <div class="navbar-header">
                    <button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                  </div>
                  <div class="megamenu-wrapper">
                    <span id="remove-megamenu" class="fa fa-times"></span>
                    <div class="megamenu-pattern" style="width: 650px">
                      <div class="container">
                        <ul class="megamenu" data-transition="slide" data-animationtime="500">
                          <li class="full-width menu-home with-sub-menu hover">
                            <p class='close-menu'></p>
                            <a href="/" class="clearfix">
                            <strong>
                            Trang Chủ
                            </strong>
                            </a>

                          </li>
                          <?php
                          $header_menu_pages = \App\Post::whereStatus('1')->where('show_in_header_menu', 1)->get();
                          ?>
                          @if($header_menu_pages->count() > 0)
                              @foreach($header_menu_pages as $page)
                              <li class="full-width">
                                  <p class='close-menu'></p>
                                  <a href="{{ route('single_page', $page->slug) }}" class="clearfix">
                                      <strong>
                                      {{ $page->title }}
                                      </strong>
                                  </a>

                              </li>
                              @endforeach
                          @endif

                          <li class="full-width">
                              <p class='close-menu'></p>
                              <a href="{{route('create_ad')}}" class="clearfix">
                                  <strong>
                                      Tạo Đấu Giá
                                  </strong>
                              </a>

                          </li>
                          <li class="full-width">
                              <p class='close-menu'></p>
                              <a href="#" class="clearfix">
                                  <strong>
                                      Sàn Đấu Giá
                                  </strong>
                              </a>

                          </li>
                          <li class="full-width">
                              <p class='close-menu'></p>
                              <a href="#" class="clearfix">
                                  <strong>
                                  Tin Tức
                                  </strong>
                              </a>

                          </li>
                          <li class="full-width">
                              <p class='close-menu'></p>
                              <a href="#" class="clearfix">
                                  <strong>
                                  Liên Hệ
                                  </strong>
                              </a>

                          </li>

                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-xs-4 col-md-9 col-lg-6">
          <div class="hidden-xs">
            <div class="text-html">
              <p>
                Hôm nay bạn muốn mua gì ?
              </p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- //Header Top -->
  <!-- Header center -->
  <div class="header-center">
    <div class="container">
      <div class="row">
        <div class="header-logo col-lg-2 col-md-2 col-sm-3 col-xs-12">
          <a href="/"><img src="{{ asset('assets/ver2/image/catalog/demo/logo/logo-3.png') }}" title="Your Store - Layout 3" alt="Your Store - Layout 3"></a>
        </div>
        <div class="header-search col-lg-5 col-md-5 col-sm-5 col-xs-5">
          <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
            <form method="GET" action="/search">
              <div id="search0" class="search input-group form-group">

                <input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Tìm kiếm" name="key">
                <span class="input-group-btn">
                <button type="submit" class="button-search btn btn-default btn-lg" name="submit_search"><i class="fa fa-search"></i><span class="hidden">Tìm kiếm</span></button>
                </span>
              </div>
              <input type="hidden" name="route" value="product/search">
            </form>
          </div>
        </div>
        <div class="header-cart col-lg-2 col-md-2 col-sm-1  col-xs-2">
          <div class="shopping_cart">
            <div id="cart" class="btn-shopping-cart">
              <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle" data-toggle="dropdown">
                <div class="shopcart">
                  <span class="handle pull-left"></span>
                  <div class="cart-info">
                    <h2 class="title-cart">Thông Báo</h2>
                    <h2 class="title-cart2 hidden">Thông Báo</h2>
                    <span class="total-shopping-cart cart-total-full">
                    <span class="items_cart">0</span>
                    </span>
                  </div>
                </div>
              </a>

              <ul class="dropdown-menu pull-right shoppingcart-box">
                  <li class="content-item" >
                    <table class="table table-striped" style="margin-bottom:10px;">
                      <tbody id="list_notify">
                      </tbody>
                    </table>
                  </li>

                </ul>


            </div>
          </div>
        </div>
            @if (Auth::guest())
            <div class="user-info-layout-3 col-lg-3 col-md-3 col-sm-3 col-xs-5">
              <div class="userinfo-block">
                <div class="user-info">
                  <a class="link-lg" href="{{ route('register') }}">Đăng Ký </a> /
                  <a class="link-lg" href="{{ route('login') }}">Đăng nhập </a>
                </div>
              </div>



            </div>
            @else
            <div class="user-info-layout-3 col-lg-3 col-md-3 col-sm-3 col-xs-5">
              <div class="userinfo-block">
                <div class="user-info">
                    <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle" data-toggle="dropdown" href="{{ route('register') }}">{{ auth()->user()->name }} </a>
                    <ul style="width: 100%;" class="dropdown-menu pull-right shoppingcart-box">
                      <li><a style="color: #000;" href="{{route('dashboard')}}"> @lang('app.dashboard') </a> </li>
                      <li>
                          <a style="color: #000;"  href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                              @lang('app.logout')
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                      </li>
                    </ul>
                    <!-- <ul class="dropdown-menu">
                      <li><a href="{{route('dashboard')}}"> @lang('app.dashboard') </a> </li>
                      <li>
                          <a href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                              @lang('app.logout')
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                      </li>
                    </ul> -->

                </div>
              </div>



            </div>

            @endif
      </div>
    </div>
  </div>
  <!-- //Header center -->
  <!-- Header bottom-->

</header>
<script>

var page_notify = 0;
$(function(){
    $.ajax({
        type : 'GET',
        url : '/notify?page='+page_notify,
        success : function (data) {
          if(data['result'] == false){
          }else{
              html = '';
              if(data.result == true){
                $.each(data.data.notify, function( index, value ) {
                  data_json = JSON.parse(value.data);
                  html +=
                    '<tr>'+
                      '<td class="text-center size-img-cart">'+
                          '<a href="/auction/'+data_json.ad_id+'/bids-auction">'+
                            '<img src="'+data_json.image+'" alt="'+data_json.title+'" title="'+data_json.title+'" class="img-thumbnail">'+
                          '</a>'+
                      '</td>'+

                      '<td class="text-left"><a href="/auction/'+data_json.ad_id+'/bids-auction">'+data_json.title+'</a>'+
                      '<br> - <small>'+data_json.time+'</small>'+
                      '</td>'+

                    '</tr>';
                });
                if(html == ''){
                  html = '<p>Chưa có thông báo nào cho bạn</p>';
                }
                $('#list_notify').html(html);
                $('.items_cart').html(data.data.notify_count);
              }



          }
        }
    });







});
var user_id = '{{ @Auth::user()->id }}';
socket.on("notify_push",function(data_client_on){
    var data_reponse = JSON.parse(data_client_on);
    if(user_id != ''){

      $.each(data_reponse, function( index, value ) {
        data_json = value;
        html = '';
        html +=
          '<tr>'+
            '<td class="text-center size-img-cart">'+
                '<a href="/auction/'+data_json.ad_id+'/bids-auction">'+
                  '<img src="'+data_json.image+'" alt="'+data_json.title+'" title="'+data_json.title+'" class="img-thumbnail">'+
                '</a>'+
            '</td>'+

            '<td class="text-left"><a href="/auction/'+data_json.ad_id+'/bids-auction">'+data_json.title+'</a>'+
            '<br> - <small>'+data_json.time+'</small>'+
            '</td>'+

          '</tr>';
        if(user_id == data_json.user_id){
            $('#list_notify').html(html + $('#list_notify').html());
            $('.items_cart').html(parseInt($('.items_cart').html())+1);
        }
      });
    }
});
</script>
