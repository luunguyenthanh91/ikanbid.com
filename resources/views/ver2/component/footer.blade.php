<footer class="footer-container typefooter-3">
  <div class="footer-has-toggle collapse" id="collapse-footer"  >
    <div class="so-page-builder">
      <section class="section_1  section-color ">
        <div class="container page-builder-ltr">
          <div class="row row_5fdf  row-style ">
            <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 col_9jik  col-style">
              <div class="module news-letter">
                <div class="so-custom-default newsletter" style="width:100%     ; background-color: #f0f0f0 ; ">
                  <div class="btn-group title-block">
                    <div class="popup-title page-heading">
                      <i class="fa fa-paper-plane-o"></i> Đăng ký để nhận tin khuyến mãi mỗi ngày
                    </div>
                    <div class="newsletter_promo">Và nhận <span>500.000 VNĐ</span>khuyến mãi phí vận chuyển.</div>
                  </div>
                  <div class="modcontent block_content">
                    <form method="post" id="signup" name="signup" class="form-group form-inline signup send-mail">
                      <div class="input-group form-group required">
                        <div class="input-box">
                          <input type="email" placeholder="Nhập địa chỉ email" value="" class="form-control" id="txtemail" name="txtemail" size="55">
                        </div>
                        <div class="input-group-btn subcribe">
                          <button class="btn btn-primary" type="submit" onclick="return subscribe_newsletter();" name="submit">
                          <i class="fa fa-envelope hidden"></i>
                          <span>Subscribe</span>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!--/.modcontent-->
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 col_l60i  col-style">
              <div class="footer-social">
                <h3 class="block-title">Follow us</h3>
                <div class="socials">
                  <a href="#" class="facebook" target="_blank">
                    <i class="fa fa-facebook"></i>
                    <p>on</p>
                    <span class="name-social">Facebook</span>
                  </a>
                  <a href="#" class="twitter" target="_blank">
                    <i class="fa fa-twitter"></i>
                    <p>on</p>
                    <span class="name-social">Twitter</span>
                  </a>
                  <a href="#" class="google" target="_blank">
                    <i class="fa fa-google-plus"></i>
                    <p>on</p>
                    <span class="name-social">Google +</span>
                  </a>
                  <a href="#" class="dribbble" target="_blank"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                  <a href="#" class="instagram" target="_blank">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                    <p>on</p>
                    <span class="name-social">Instagram</span>
                  </a>
                  <a href="#" class="pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>   <a href="#" class="linkedIn" target="_blank"><i class="fa fa-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section_2  section-color ">
        <div class="container page-builder-ltr">
          <div class="row row_dsw3  row-style  row-color ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_itqc  col-style">
              <div class="clearfix bonus-menus bonus-menu-4">
                <ul>
                  <li class="item secure col-md-3">
                    <div class="icon">
                    </div>
                    <div class="text">
                      <h5><a href="#">Đăng Ký</a></h5>
                      <p>Xem và đấu giá trực tiếp</p>
                      <p> Cam kết bảo mật 100%</p>
                    </div>
                  </li>

                  <li class="item trustpay col-md-3">
                    <div class="icon">
                    </div>
                    <div class="text">
                      <h5><a href="#">Đấu giá</a></h5>
                      <p>Tham gia đấu giá</p>
                      <p> Đợi đấu giá thắng thầu giữa các người chơi </p>
                    </div>
                  </li>


                  <li class="item value col-md-3">
                    <div class="icon">
                    </div>
                    <div class="text">
                      <h5><a href="#">Thanh Toán</a></h5>
                      <p>Trúng thầu</p>
                      <p>Sau khi trúng thầu bạn cần thanh toán cho người bán</p>
                    </div>
                  </li>
                  <li class="item delivery col-md-3">
                    <div class="icon">
                    </div>
                    <div class="text">
                      <h5><a href="#">Vận chuyển</a></h5>
                      <p>Nhận được tiền từ người thắng thầu </p>
                      <p>Giao hàng trong vòng 24h kể từ khi thắng thầu</p>
                    </div>
                  </li>
                  <li class="item help col-md-3">
                    <div class="icon">
                    </div>
                    <div class="text">
                      <h5><a href="#">Hỗ Trợ</a></h5>
                      <p>Đảm bảo quyền lợi 2 bên </p>
                      <p>Kết thúc giao dịch , mọi vấn đề liên quan vẫn được hỗ trợ viên chăm sóc.</p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>
  </div>
  <div class="footer-toggle hidden-lg hidden-md">
    <a class="showmore collapsed" data-toggle="collapse" href="#collapse-footer" aria-expanded="false" aria-controls="collapse-footer">
    <span class="toggle-more"><i class="fa fa-angle-double-down"></i>Show More</span>
    <span class="toggle-less"><i class="fa fa-angle-double-up"></i>Show less</span>
    </a>
  </div>
  <div class="footer-bottom ">
    <div class="container">
      <div class="row">
        <div class="col-md-7  col-sm-7 copyright">
            Bản Quyền @ikanbid 2019 - 2020 . Liên hệ Ban Quản Trị Website Để Biết Thêm Chi Tiết.
        </div>
        <div class="col-md-5 col-sm-5 paymen">
          <img src="{{ asset('assets/ver2/image/catalog/demo/payment/payments-1.png') }}"  alt="imgpayment">
        </div>
      </div>
    </div>
  </div>
</footer>
